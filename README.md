**SharpEnd CLI**

This command line interface helps start SharpEnd dev projects using templates. These templates allow us to maintain consistency between projects and save time during initialisation.

The CLI is published as an NPX tool. To use it: 

- cd into the desired root directory
- Run 'npx sharpend-cli'
- Follow prompts in the terminal to set up a customised SharpEnd dev project

----------------------------------------------

Based on the code of @kerimhudson/create
