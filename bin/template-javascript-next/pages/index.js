import styles from '../styles/Home.module.css'
import Image from 'next/image'

export default function Home() {

  const globalWrapper = {
    width: '100%',
    height: '100vh',
    backgroundColor: 'black',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }

  const imageContainer = {
    width: '300px',
    height: '300px',
    position: 'relative'
  }

  const text = {
    color: 'white',
    marginTop: '60px',
    fontWeight: '200',
    fontSize: '24px'
  }

  return (
    <div style={globalWrapper}>
      <div style={imageContainer} className={styles.animation}>
        <Image src="/logo.png" layout="fill" objectFit='contain'/> 
      </div>
      <h1 style={text}>New SharpEnd Dev Project</h1>
    </div>
  )
}