import type { NextPage } from 'next'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {

  return (
    <div className={styles.globalWrapper} >
      <div className={`${styles.imageContainer} ${styles.animation}`}>
        <Image src="/logo.png" layout="fill" objectFit='contain'/> 
      </div>
      <h1 className={styles.text}>New SharpEnd Dev Project</h1>
    </div>
  )
}

export default Home
