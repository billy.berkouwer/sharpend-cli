#! /usr/bin/env node

// @ts-check
const fs = require("fs");
const path = require("path");
const argv = require("minimist")(process.argv.slice(2));
const { prompt } = require("enquirer");
const { yellow, cyan, stripColors } = require("kolorist");

const cwd = process.cwd()

const TEMPLATES = [yellow("javascript-next"), cyan("typescript-next")];

const renameFiles = {
  _gitignore: ".gitignore",
};

async function init() {
  let targetDir = argv._[0];
  if (!targetDir) {
    /**
     * @type {{ name: string }}
     */
    const { name } = await prompt({
      type: "input",
      name: "name",
      message: `Project name:`,
      initial: "next-project",
    });
    targetDir = name;
  }

  // path of file
  const root = path.join(cwd, targetDir);
  console.log(`\nScaffolding project in ${root}...`);

  // if path name of new file exists
  if (!fs.existsSync(root)) {
    fs.mkdirSync(root, { recursive: true });
  } else {
    const existing = fs.readdirSync(root);
    if (existing.length) {
      /**
       * @type {{ yes: boolean }}
       */
      const { yes } = await prompt({
        type: "confirm",
        name: "yes",
        initial: "Y",
        message:
          `Target directory ${targetDir} is not empty.\n` +
          `Remove existing files and continue?`,
      });
      if (yes) {
        emptyDir(root);
      } else {
        return;
      }
    }
  }

  // determine template
  let template = argv.t || argv.template;
  let message = "Select a template:";
  let isValidTemplate = false;

  // --template expects a value
  if (typeof template === "string") {
    const availableTemplates = TEMPLATES.map(stripColors);
    isValidTemplate = availableTemplates.includes(template);
    message = `${template} isn't a valid template. Please choose from below:`;
  }

  if (!template || !isValidTemplate) {
    /**
     * @type {{ t: string }}
     */
    const { t } = await prompt({
      type: "select",
      name: "t",
      message,
      choices: TEMPLATES,
    });
    template = stripColors(t);
  }

  const templateDir = path.join(__dirname, `template-${template}`);

  const write = (file, content) => {
    const targetPath = renameFiles[file]
      ? path.join(root, renameFiles[file])
      : path.join(root, file);
    if (content) {
      fs.writeFileSync(targetPath, content);
    } else {
      copy(path.join(templateDir, file), targetPath);
    }
  };

  const files = fs.readdirSync(templateDir);
  for (const file of files.filter((f) => f !== "package.json")) {
    write(file);
  }

  // INSTALL COMMON PACKAGES

  const packageJson = `
      {
        "name": "",
        "version": "0.1.0",
        "private": true,
        "scripts": {
          "dev": "next dev",
          "build": "next build",
          "start": "next start",
          "lint": "next lint"
        },
        "dependencies": {
          "next": "^12.1.5",
          "react": "^18.1.0",
          "react-dom": "^18.1.0"
        },
        "devDependencies": {
          "eslint": "8.14.0",
          "eslint-config-next": "12.1.5"
        }
      }`;

  const packageOptions = {
    contentful: {
      name: "contentful",
      version: "^9.1.27",
    },
    amplify: {
      name: "aws-amplify",
      version: "^4.3.20",
    },
    gsap: {
      name: "gsap",
      version: "^3.10.4",
    },
    googleAnalytics: {
      name: "@analytics/google-analytics",
      version: "^0.5.3",
    },
  };

  let packageMessage = "Do you require ";
  const obj = JSON.parse(packageJson);
  const key = Object.keys(packageOptions);

  for (const el in key) {
    const name = packageOptions[key[el]].name;
    const version = packageOptions[key[el]].version;

    /**
     * @type {{ packagePrompt: string }}
     */

    const { packagePrompt } = await prompt({
      type: "confirm",
      name: "packagePrompt",
      message: packageMessage + key[el] + "?",
      initial: "Y",
    });
    if (packagePrompt) {
      console.log(`${name} added`);
      obj.dependencies[name] = version;
    }
  }

  const json = JSON.stringify(obj);

  fs.writeFile(`${root}/package.json`, json, "utf8", function (err) {
    if (err) throw err;
    console.log("complete");
  });

  // check package manager

  const pkgManager = /yarn/.test(process.env.npm_execpath) ? "yarn" : "npm";

  console.log(`\nDone. Now run:\n`);

  if (root !== cwd) {
    console.log(`  cd ${path.relative(cwd, root)}`);
  }
  console.log(`  ${pkgManager === "yarn" ? `yarn` : `npm install`}`);
  console.log(`  ${pkgManager === "yarn" ? `yarn dev` : `npm run dev`}`);
  console.log();
}

function copy(src, dest) {
  const stat = fs.statSync(src);
  if (stat.isDirectory()) {
    copyDir(src, dest);
  } else {
    fs.copyFileSync(src, dest);
  }
}

function copyDir(srcDir, destDir) {
  fs.mkdirSync(destDir, { recursive: true });
  for (const file of fs.readdirSync(srcDir)) {
    const srcFile = path.resolve(srcDir, file);
    const destFile = path.resolve(destDir, file);
    copy(srcFile, destFile);
  }
}

function emptyDir(dir) {
  if (!fs.existsSync(dir)) {
    return;
  }
  for (const file of fs.readdirSync(dir)) {
    const abs = path.resolve(dir, file);
    // baseline is Node 12 so can't use rmSync :(
    if (fs.lstatSync(abs).isDirectory()) {
      emptyDir(abs);
      fs.rmdirSync(abs);
    } else {
      fs.unlinkSync(abs);
    }
  }
}

init().catch((e) => {
  console.error(e);
});

exports.printMsg = function() {
  console.log("Export message");
}